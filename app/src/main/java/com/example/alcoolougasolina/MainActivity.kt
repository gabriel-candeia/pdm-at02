package com.example.alcoolougasolina

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    var percentual: Double = 0.7
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i("PDM24.1","No onCreate, $percentual")

        percentual=0.7
        val btCalc: Button = findViewById(R.id.btCalcular)
        val textMsg: TextView = findViewById(R.id.textMsg)
        val swPercentual: Switch = findViewById(R.id.swPercentual)

        if(savedInstanceState!=null){
            percentual = savedInstanceState.getDouble("percentual")
            swPercentual.text = String.format("%d%%",(percentual*100).toInt())
            textMsg.text = savedInstanceState.getString("textMsg")
        }

        swPercentual.setOnClickListener(View.OnClickListener {
            percentual = if(percentual==0.75) 0.7 else 0.75
            swPercentual.text = String.format("%d%%",(percentual*100).toInt())
            Log.d("PDM24","click em swPercentual, $percentual")
        })

        btCalc.setOnClickListener(View.OnClickListener {
            //código do evento
            val edGasolina: EditText = findViewById(R.id.edGasolina)
            val precoGasolina: Double? = edGasolina.text.toString().toDoubleOrNull()

            val edAlcool: EditText = findViewById(R.id.edAlcool)
            val precoAlcool: Double? = edAlcool.text.toString().toDoubleOrNull()

            if(precoGasolina==null || precoAlcool==null) {
                textMsg.text = "Preencha o preço dos dois combustíveis!"
            }
            else if(precoAlcool <= percentual*precoGasolina){
                textMsg.text = "O álcool é mais rentável!"
            }
            else{
                textMsg.text = "A gasolina é mais rentável!"
            }

            Log.d("PDM24","click em btCalcular, $percentual")
        })
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putDouble("percentual",percentual)
        outState.putString("textMsg",findViewById<TextView>(R.id.textMsg).text.toString())
    }
    override fun onResume(){
        super.onResume()
        Log.d("PDM24","No onResume, $percentual")
    }
    override fun onStart(){
        super.onStart()
        Log.v("PDM24","No onStart")
    }
    override fun onPause(){
        super.onPause()
        Log.e("PDM24","No onPause")
    }
    override fun onStop(){
        super.onStop()
        Log.w("PDM24","No onStop")
    }
    override fun onDestroy(){
        super.onDestroy()
        Log.wtf("PDM24","No Destroy")
    }
}